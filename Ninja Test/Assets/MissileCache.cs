﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Missile game objects are pre-cached to avoid creating and destroying them during play
// which would potentially lead to stuttering due to Unity's garbage collection

public class MissileCache : MonoBehaviour {

	private const int CACHE_SIZE = 100;
	private static Vector3 relativeToPlayer;

	public class CachedMissile {
		// Class used to store info about one pre-cached missile. These classes are
		// maintained in a linked list which is dynamically re-ordered to keep active
		// objects at the end and inactive objects at the beginning so the first object
		// will always be the next one that can be used.
		public CachedMissile prev = null, next = null;
		public GameObject gameObject;
		public Missile missileScript;
		public bool active = false;
	}

	// Our linked list uses dummy first and last objects to bookend the objects that
	// we'll actually be using. This is so we don't have to do null checking because every
	// object we're using is guaranteed to have a valid next and prev.
	private static CachedMissile first = new CachedMissile(), last = new CachedMissile();
	private static CachedMissile cachedMissile;

	void Start () {
		relativeToPlayer.z = 0;
	}

	public static void Init() {
		first.next = last;
		last.prev = first;

		int count = CACHE_SIZE;
		Transform missile = Resources.Load ("Missile", typeof(Transform)) as Transform;

		while (count > 0) {
			// Create one missile object and insert it as the second item in the linked list.
			// The first item is just a bookend so the second item is the one we'll use when
			// we want to activate an object.
			cachedMissile = new CachedMissile ();
			cachedMissile.next = first.next;
			cachedMissile.prev = first;
			first.next = cachedMissile;
			cachedMissile.next.prev = cachedMissile;

			// Create an instance of the missile prefab. Store its GameObject and its Missile
			// script in the cache object. This is so we don't have to do lookups in play.
			cachedMissile.gameObject = Instantiate (missile).gameObject;
			cachedMissile.missileScript = cachedMissile.gameObject.GetComponent<Missile> ();
			cachedMissile.missileScript.cachedMissile = cachedMissile;
			cachedMissile.missileScript.Init ();
			cachedMissile.gameObject.SetActive (false);
			count--;
		}
	}

	public static CachedMissile GetMissile() {
		// The first cached missile (second in the linked list) is removed from the beginning of
		// the list and re-inserted at the end of the list. The function returns the cached missile.
		cachedMissile = first.next;
		cachedMissile.prev.next = cachedMissile.next;
		cachedMissile.next.prev = cachedMissile.prev;
		cachedMissile.prev = last.prev;
		cachedMissile.next = last;
		cachedMissile.prev.next = cachedMissile;
		cachedMissile.next.prev = cachedMissile;
		cachedMissile.active = true;
		return cachedMissile;
	}

	public static void Release(CachedMissile cm) {
		// Mark a cached missile as inactive, remove it from the linked list, and re-insert it at the
		// beginning of the list so it can be re-used next time a missile is needed.
		cm.gameObject.SetActive (false);
		cm.active = false;
		cm.prev.next = cm.next;
		cm.next.prev = cm.prev;
		cm.prev = first;
		cm.next = first.next;
		cm.prev.next = cm;
		cm.next.prev = cm;
	}

	void Update () {
	}

//	public static void RemoveIfCloseToPlayer(float range) {
//		// Cycle through all active missiles, check if they're within range of the player, and
//		// deactivate them if they are. All active missiles are at the end of the list so we
//		// can simply start with the last item and work backwards, checking each missile, until
//		// we find one that is already inactive. Missiles earlier in the list are already
//		// inactive so we don't need to check them.
//		CachedMissile cm = last.prev, temp;
//		while ((cm != null) && cm.active) {
//			relativeToPlayer = Player.pos - cm.missileScript.pos;
//			if (relativeToPlayer.magnitude < range) {
//				temp = cm.prev;
//				Release (cm);
//				cm = temp;
//				continue;
//			}
//			cm = cm.prev;
//		}
//	}

//	public static void ChangeBearingIfCloseToPlayer(float range, Vector3 newBearing) {
//		// Cycle through all active missiles, check if they're within range of the player, and
//		// if they are then change their bearing.
//		CachedMissile cm = last.prev;
//		while (cm.active) {
//			relativeToPlayer = Player.pos - cm.missileScript.pos;
//			if (relativeToPlayer.magnitude < range) {
//				cm.missileScript.bearing = newBearing;
//			}
//			cm = cm.prev;
//		}
//	}
}
