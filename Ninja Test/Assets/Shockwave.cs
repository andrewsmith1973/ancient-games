﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shockwave : MonoBehaviour {

	// The shockwave is a plane with a circle texture on it that expands until it reaches
	// a maximum size and then disappears. While it's visible, if any missiles move within
	// it, they deflect at the last-swiped vector.

	Coroutine coroutine = null;
	public static float size;
	private const float EXPAND_SPEED = 6;
	private const float TARGET_SIZE = 4;

	void Start () {
		gameObject.SetActive (false);
	}
	
	void Update () {
	}

	public void Trigger() {
		if (coroutine != null) {
			StopCoroutine (coroutine);
		}

		gameObject.SetActive (true);
		coroutine = StartCoroutine (Expand ());
	}

	IEnumerator Expand() {
		Shockwave.size = 0;
		do {
			Shockwave.size += EXPAND_SPEED * Time.deltaTime;
			if (Shockwave.size >= TARGET_SIZE) {
				Shockwave.size = TARGET_SIZE;
				coroutine = null;
			}
			transform.localScale = Vector3.one * (Shockwave.size * 2);
			yield return null;
		} while (coroutine != null);
		gameObject.SetActive (false);
		Shockwave.size = 0;
	}
}
