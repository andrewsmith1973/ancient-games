﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

	private Enemies.Position randomPos, temp;
	private Vector3 pos;
	private const float ROTATE_SPEED = 90;
	private const float APPEAR_SPEED = 1;
	private float scale = 0;
	private float appearDelay;
	private bool coroutineStarted = false;
	private float sin = 0, cos = 0, sinrad, cosrad;
	private float SIN_SPEED = 360;
	private float COS_SPEED = 180;
	private float SIN_SIZE = 0.25f;
	private Vector3 wobblePos;
	private Quaternion rot;
	private float ROT_WOBBLE_SIZE = 15f;
	private Quaternion rotWobble;
	private const int NUM_DEBRIS = 20;
	private Transform[] debrisObjects = new Transform[NUM_DEBRIS];
	private Debris[] debrisScripts = new Debris[NUM_DEBRIS];
	private Transform plus10;
	private Points plus10Script;

	void Start () {
	}

	public void Init() {
		// The wobble values are for making the enemy move around so it appears active.
		// We store the enemy's root position in pos and its display position in wobblePos.
		// The enemy only ever wobbles in the x and z axis.
		wobblePos.y = 0;

		// Pre-cache debris chunks.
		Transform debris = Resources.Load ("Debris", typeof(Transform)) as Transform;
		for (int i = 0; i < NUM_DEBRIS; i++) {
			debrisObjects [i] = Instantiate (debris);
			debrisObjects [i].gameObject.SetActive (false);
			debrisScripts [i] = debrisObjects [i].GetComponent<Debris> ();
			debrisScripts [i].Init ();
		}

		// The "+10" score that pops up when an enemy dies.
		plus10 = transform.Find ("Plus10");
		plus10.SetParent (null);
		plus10.gameObject.SetActive (false);
		plus10Script = plus10.GetComponent<Points> ();
	}

	void Update () {
		// Enemies have a random appearDelay which counts down. If it's still >0 then
		// the enemy stays hidden.
		appearDelay -= Time.deltaTime;
		if (appearDelay > 0) {
			return;
		}

		// When enemies appear they start at scale 0 and enlarge over time.
		if (scale < 1) {
			scale += APPEAR_SPEED * Time.deltaTime;
			if (scale > 1) {
				scale = 1;
			}
			transform.localScale = Vector3.one * scale;
		}

		// Wobble the enemy to make it look active. A bit of wobbly rotation too.
		// (Obvious optimisations possible here!)
		sin += SIN_SPEED * Time.deltaTime;
		cos += COS_SPEED * Time.deltaTime;
		sinrad = sin * Mathf.Deg2Rad;
		cosrad = cos * Mathf.Deg2Rad;
		wobblePos = pos;
		wobblePos.x += Mathf.Sin (sinrad) * SIN_SIZE * Mathf.Sin(cosrad);
		wobblePos.z += Mathf.Cos (sinrad) * SIN_SIZE * Mathf.Cos(cosrad);
		transform.position = wobblePos;
		rotWobble = Quaternion.Euler(0, ROT_WOBBLE_SIZE * Mathf.Sin (sinrad) * Mathf.Sin(cosrad), 0);
		transform.rotation = rotWobble * rot;
	}

	public void Activate(Enemies.Position _randomPos) {
		// Reset everything and initialise the enemy at a given position.
		scale = 0;
		transform.localScale = Vector3.zero;
		appearDelay = Random.Range (1f, 5f);
		randomPos = _randomPos;
		pos = randomPos.pos;
		pos = Enemies.ShufflePosition (pos);
		pos = Maths.ScreenPosToWorldPos (pos);
		randomPos.occupied = true;
		transform.position = pos;
		transform.LookAt (Player.pos);
		rot = transform.rotation;

		// The coroutine to launch missiles runs constantly, even if the enemy has died
		// and is currently respawning.
		if (!coroutineStarted) {
			StartCoroutine (LaunchMissiles ());
			coroutineStarted = true;
		}
	}

	IEnumerator Score(Vector3 pos) {
		// Coroutine for when the enemy has been destroyed. We've already triggered an
		// explosion sound effect, so we wait a moment before
		// showing the +10 score popup and playing another sound effect.
		yield return new WaitForSeconds(0.25f);
		Audio.Score();
		plus10Script.Activate(pos);
	}

	void OnTriggerEnter(Collider col) {
		// disable the pre-cached missile so it can be re-allocated in future
		Missile missileScript = col.GetComponent<Missile> ();
		if (!missileScript.rebounded) {
			return;
		}
		MissileCache.Release (missileScript.cachedMissile);

		// Play explosion sound effect
		Audio.EnemyHit();
		// Launch a coroutine that will wait a moment before displaying the score and playing
		// another sound effect.
		StartCoroutine (Score (wobblePos));

		// Spawn debris chunks.
		for (int i = 0; i < NUM_DEBRIS; i++) {
			debrisScripts [i].Activate (
				wobblePos.x,
				wobblePos.z,
				missileScript.bearing
			);
		}

		// and respawn the enemy in a new position
		temp = randomPos;
		Activate (Enemies.GetRandomPosition ());
		temp.occupied = false;
	}

	IEnumerator LaunchMissiles() {
		// Coroutine to launch missiles at a random interval.
		while (true) {
			while (scale <= 0) {
				// Don't launch if the enemy is still spawning.
				yield return null;
			}
			yield return new WaitForSeconds(Random.Range(3f, 6f));
			MissileCache.GetMissile ().missileScript.Activate (wobblePos.x, wobblePos.z);
		}
	}
}
