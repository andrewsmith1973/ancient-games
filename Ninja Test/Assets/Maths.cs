﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Maths : MonoBehaviour {

	private static Plane groundPlane;
	private static Ray ray;
	private static float rayDist;
	private static Plane leftPlane, rightPlane, topPlane, bottomPlane;

	void Start () {
	}

	void Update () {
	}

	public static void Init() {
		// groundPlane is used for ray intersections
		groundPlane = new Plane (Vector3.up, Vector3.zero);

		// edge planes are used for checking if a missile has left the view frustrum
		Vector3 bottomLeft = ScreenPosToWorldPos(new Vector3(0, 0, 0));
		Vector3 topLeft = ScreenPosToWorldPos(new Vector3(0, Screen.height, 0));
		Vector3 bottomRight = ScreenPosToWorldPos(new Vector3(Screen.width, 0, 0));
		Vector3 topRight = ScreenPosToWorldPos(new Vector3(Screen.width, Screen.height, 0));
			
		leftPlane = new Plane (
			bottomLeft,
			bottomLeft + Vector3.up,
			topLeft
		);

		rightPlane = new Plane (
			topRight,
			topRight + Vector3.up,
			bottomRight
		);

		topPlane = new Plane (
			topLeft,
			topLeft + Vector3.up,
			topRight
		);

		bottomPlane = new Plane (
			bottomRight,
			bottomRight + Vector3.up,
			bottomLeft
		);
	}

	public static Vector3 ScreenPosToWorldPos(Vector3 screenPos) {
		// Casts a ray from the camera position through a point on screen and returns the
		// intersection point with the ground plane.
		ray = Camera.main.ScreenPointToRay (screenPos);
		groundPlane.Raycast (ray, out rayDist);
		return ray.GetPoint (rayDist);
	}

	public static bool IsPointOutsideViewFrustrum(Vector3 point) {
		// If an object's world position is on the negative side of any plane then it's
		// outside the view frustrum and the object can be removed.

		if (!leftPlane.GetSide (point)) {
			return true;
		}

		if (!rightPlane.GetSide (point)) {
			return true;
		}

		if (!topPlane.GetSide (point)) {
			return true;
		}

		if (!bottomPlane.GetSide (point)) {
			return true;
		}

		return false;
	}
}
