﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour {

	public GameObject playerGO;
	private Player playerScript;
	private WaitForSeconds wfs = new WaitForSeconds (3);
	private float deg, rad, x, y, z;
	private Ray ray;
	private float rayDist;
	private Vector3 worldPos, screenPos;
	private int random;

	void Start () {
		// Call initialisation functions
		Camera.main.GetComponent<SetAspectRatio> ().Init ();
		Maths.Init();
		playerScript = playerGO.GetComponent<Player> ();
		playerScript.Init ();
		MissileCache.Init ();
		Enemies.Init ();
		Enemies.SpawnEnemies ();
		// Start a coroutine to spawn a new missile at regular intervals
		StartCoroutine (SpawnMissiles ());
	}
	
	void Update () {
	}

	private IEnumerator SpawnMissiles() {
		// We choose to spawn a new missile randomly at the top, left or right of the screen.
		// If it's on the left or right then we spawn it somewhere between the top and level
		// with the player.
		while (true) {
			switch (Random.Range(0, 3)) {
			case 0:
				screenPos.x = 0;
				screenPos.y = Random.Range (Player.screenPosY, Screen.height);
				break;
			case 1:
				screenPos.x = Screen.width;
				screenPos.y = Random.Range (Player.screenPosY, Screen.height);
				break;
			case 2:
				screenPos.x = Random.Range (0, Screen.width);
				screenPos.y = Screen.height;
				break;
			}

			// Then we cast a ray in to the screen, intersecting with the grid plane, to find
			// where in world space to spawn the missile. This avoids missiles appearing outside
			// the view frustrum.
			// NOTE: If this game was going to be played competitively such as with a high score
			// table then it would actually be better to spawn the missiles consistently, rather
			// than dependent upon the player's screen size. But seeing as this is a programming
			// test I thought you'd want to see some maths :-)
			worldPos = Maths.ScreenPosToWorldPos (screenPos);
			MissileCache.GetMissile ().missileScript.Activate (worldPos.x, worldPos.z);
			yield return wfs;
		}
	}
}
