﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Audio : MonoBehaviour {

	public AudioClip playerHit;
	public AudioClip score;
	public AudioClip enemyHit;
	public AudioClip missileDeflect;
	public AudioClip swipe;

	private static AudioSource audioSource;
	private static Audio audio;

	void Start () {
		audio = this;
		audioSource = GetComponent<AudioSource> ();
	}
	
	void Update () {
	}

	public static void PlayerHit() {
		audioSource.PlayOneShot (audio.playerHit);
	}

	public static void Score() {
		audioSource.PlayOneShot (audio.score);
	}

	public static void EnemyHit() {
		audioSource.PlayOneShot (audio.enemyHit);
	}

	public static void MissileDeflect() {
		audioSource.PlayOneShot (audio.missileDeflect);
	}

	public static void Swipe() {
		audioSource.PlayOneShot (audio.swipe);
	}
}
