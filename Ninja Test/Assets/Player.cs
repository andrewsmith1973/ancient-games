﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

	public static Vector3 pos;
	public static float screenPosY;
	public static Shockwave shockwaveScript;
	private const float SHRINK_SPEED = 1;
	private float size = 1;
	private Vector3 nudge = Vector3.zero;
	private float nudgeScale = 0;
	private const float NUDGE_RETURN_SPEED = 4;

	void Start () {
	}

	public void Init() {
		pos = transform.position;
		// screenPosY is used in Game->SpawnMissiles() to make sure missiles aren't
		// spawned behind the player
		Vector3 screenPos = Camera.main.WorldToScreenPoint (pos);
		screenPosY = screenPos.y + ((Screen.height - screenPos.y) * 0.5f);
		shockwaveScript = transform.Find ("Shockwave").GetComponent<Shockwave> ();
	}

	void Update () {
		// When the player is hit, the cube jumps in size a bit and gradually returns
		// to normal size.
		if (size > 1) {
			size -= SHRINK_SPEED * Time.deltaTime;
			if (size < 1) {
				size = 1;
			}
			transform.localScale = Vector3.one * size;
		}

		// The cube also gets nudged by any missiles that hit it, and then gradually
		// returns to its root position.
		if (nudgeScale > 0) {
			nudgeScale -= NUDGE_RETURN_SPEED * Time.deltaTime;
			if (nudgeScale < 0) {
				nudgeScale = 0;
			}

			transform.position = pos + (nudge * nudgeScale);
		}
	}

	void OnTriggerEnter(Collider col) {
		// if a missile collides with the player then disable the pre-cached missile
		// and allow it to be re-allocated in future
		Missile missileScript = col.GetComponent<Missile> ();
		// Nudge the player in the direction that the incoming missile was heading.
		// There are two steps to this:
		// 1. Because the player may already be nudged, we'll adjust the nudge vector
		// according to the current nudgeScale to "bake" it.
		// 2. We'll then add the new nudge amount, and normalise the result so the
		// nudge vector is never longer than 1 unit.
		nudge *= nudgeScale;
		nudge += missileScript.bearing;
		nudge.Normalize ();
		nudgeScale = 0.5f;
		MissileCache.Release (missileScript.cachedMissile);
		Audio.PlayerHit ();
		// Give the player size a jump. It will return to normal size in Update()
		size = 1.25f;
	}

	public static void TriggerShockwave() {
		// Called when the player swipes.
		shockwaveScript.Trigger ();
		Audio.Swipe ();
	}
}
