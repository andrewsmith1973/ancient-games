﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlienAnimation : MonoBehaviour {

	// Each enemy object contains two child objects which are an alien in different poses.
	// We swap between the two children every FRAME_LENGTH to animate.

	private GameObject alien1, alien2;
	private bool animFirstFrame = true;
	private float timer = 0;
	private const float FRAME_LENGTH = 0.5f;

	void Start () {
		alien1 = transform.Find ("alien1").gameObject;
		alien2 = transform.Find ("alien2").gameObject;
	}
	
	void Update () {
		timer += Time.deltaTime;
		while (timer > FRAME_LENGTH) {
			timer -= FRAME_LENGTH;
			animFirstFrame = !animFirstFrame;
		}

		alien1.SetActive (animFirstFrame);
		alien2.SetActive (!animFirstFrame);
	}
}
