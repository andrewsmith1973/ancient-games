﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemies : MonoBehaviour {

	// Most of what happens here is tracking spawn/respawn positions for the enemies so
	// they never overlap each other.

	public class Position {
		public Vector3 pos;
		public bool occupied = false;
	}

	// Define the area of the screen that we want enemies to respawn in, and how that
	// area should be split up (NUM_HORIZ cells * NUM_VERT cells)
	private const int NUM_HORIZ = 5;
	private const int NUM_VERT = 4;
	private const float SCREEN_LEFT = 0.1f;
	private const float SCREEN_RIGHT = 0.9f;
	private const float SCREEN_TOP = 0.9f;
	private const float SCREEN_BOTTOM = 0.5f;
	private static float positionWidth, positionHeight;
	private static float positionWidthHalf, positionHeightHalf;
	private static float positionWidthQuarter, positionHeightQuarter;

	private static int NUM_POSITIONS = NUM_HORIZ * NUM_VERT;
	private static Position[] randomPositions = new Position[NUM_POSITIONS];
	private static int nextPosition = 0;

	private static Position randomPos;
	private static Vector3 pos;

	private const int NUM_ENEMIES = 5;

	void Start () {
	}
	
	void Update () {
	}

	public static void Init() {
		float width = Screen.width * (SCREEN_RIGHT - SCREEN_LEFT);
		float height = Screen.height * (SCREEN_TOP - SCREEN_BOTTOM);
		float left = Screen.width * SCREEN_LEFT;
		float bottom = Screen.height * SCREEN_BOTTOM;
		positionWidth = width / (float)NUM_HORIZ;
		positionHeight = height / (float)NUM_VERT;
		positionWidthHalf = positionWidth * 0.5f;
		positionHeightHalf = positionHeight * 0.5f;
		positionWidthQuarter = positionWidth * 0.25f;
		positionHeightQuarter = positionHeight * 0.25f;
		int index;

		for (int x = 0; x < NUM_HORIZ; x++) {
			for (int y = 0; y < NUM_VERT; y++) {
				index = (NUM_HORIZ * y) + x;
				randomPositions [index] = new Position ();
				randomPositions [index].pos = new Vector3 (
					left + positionWidth * (float)x,
					bottom + positionHeight * (float)y,
					0);
			}
		}

		// The randomPositions array contains all of the spawn points in order from left to right
		// and bottom to top. Now we randomise the array. To limit searching, enemies will always
		// spawn at the next available spot, but because the array of spots is randomised it will
		// appear that the enemy is respawning randomly.
		int r;
		Position temp;
		for (int i = randomPositions.Length - 1; i > 0; i--) {
			r = Random.Range (0, i);
			temp = randomPositions [r];
			randomPositions [r] = randomPositions [i];
			randomPositions [i] = temp;
		}
	}

	public static Position GetRandomPosition() {
		// Return the Position object for the next unoccupied spawn point
		do {
			randomPos = randomPositions [nextPosition];
			nextPosition++;
			if (nextPosition == NUM_POSITIONS) {
				nextPosition = 0;
			}
		} while (randomPos.occupied);
			
		return randomPos;
	}

	public static Vector3 ShufflePosition(Vector3 pos) {
		// The Vector3 stored in each Position object is the bottom left of the space
		// that the enemy can spawn in. This function takes a Vector3 and
		// moves it to somewhere random in the middle 50% of the spawn space.
		pos.x += positionWidthHalf;
		pos.y += positionHeightHalf;
		pos.x += Random.Range (-positionWidthQuarter, positionWidthQuarter);
		pos.y += Random.Range (-positionHeightQuarter, positionHeightQuarter);
		return pos;
	}

	public static void SpawnEnemies() {
		// Spawn the initial enemies. Enemy->Activate sets a random delay for each enemy to
		// appear so they won't all appear straight away.
		Transform enemy = Resources.Load ("Enemy", typeof(Transform)) as Transform;
		Transform t;;

		for (int i = 0; i < NUM_ENEMIES; i++) {
			t = Instantiate (enemy);
			randomPos = GetRandomPosition ();
			t.GetComponent<Enemy> ().Init ();
			t.GetComponent<Enemy> ().Activate (randomPos);
		}
	}
}
