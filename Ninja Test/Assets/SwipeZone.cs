﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeZone : MonoBehaviour {

	// If the player clicks in the swipe zone then we store the click position. When they lift
	// their finger we check the distance they've moved, and if it's more than 1/10th of the
	// screen width then we consider that a swipe.

	private Vector3 start, end, vec;
	private float minimumSwipe;
	public static Vector3 newBearing;

	void Start () {
		minimumSwipe = Screen.width / 10;
	}
	
	void Update () {
	}

	void OnMouseDown() {
		start = Input.mousePosition;
	}

	void OnMouseUp() {
		// The player has swiped in screen space but we need to convert that to a new bearing
		// in world space. Convert start and end screen points to world points on the ground plane
		// and then calculate the normalised vector.

		end = Input.mousePosition;
		vec = end - start;
		if (vec.magnitude < minimumSwipe) {
			return;
		}

		start = Maths.ScreenPosToWorldPos (start);
		end = Maths.ScreenPosToWorldPos (end);
		newBearing = end - start;
		newBearing.Normalize ();
		Player.TriggerShockwave ();
	}
}
