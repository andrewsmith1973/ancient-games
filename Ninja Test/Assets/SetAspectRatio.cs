﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetAspectRatio : MonoBehaviour {

	void Start () {
	}

	public void Init() {
		// We want to keep the same view width regardless of screen resolution, so we adjust
		// the aspect ratio and field of view to show more or less height.
		float aspect = (float)Screen.width / (float)Screen.height;
		Camera.main.aspect = aspect;
		Camera.main.fieldOfView = 35 / aspect;
	}
	
	void Update () {
	}
}
