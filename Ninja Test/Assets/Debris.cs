﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Debris : MonoBehaviour {

	// Chunks of debris that are thrown out when an enemy explodes. Each debris object consists
	// of two children: A shadow plane that stays at ground level, and the debris chunk. The whole
	// object is moved on the x and z axis, while only the debris is moved on the y axis with
	// simulated gravity and bouncing.

	private Vector3 bearing;
	private const float MAX_SPEED = 5f;
	private float speed;
	private Vector3 pos, debrisPos;
	private const float MAX_LIFETIME = 2f;
	private float lifetime, lifetimeReset;
	private float vely, gravity = -10f;
	private Vector3 missileBearing;
	private Transform debris;

	void Start () {
	}

	public void Init() {
		// Each enemy pre-caches a hard-coded number of debris objects and calls Init for each
		// of them.
		debris = transform.Find ("Debris");
		debris.localScale = Vector3.one * Random.Range (0.15f, 0.25f);
		bearing.y = 0;
		transform.rotation = Quaternion.Euler (0, Random.Range (360f, 360f), 0);
		speed = Random.Range(0.25f, 1f) * MAX_SPEED;
		lifetimeReset = Random.Range(0.5f, 1f) * MAX_LIFETIME;
		float deg = Random.Range (0f, 360f);
		float rad = deg * Mathf.Deg2Rad;
		bearing.x = Mathf.Sin (rad);
		bearing.z = Mathf.Cos (rad);
	}

	void Update () {
		// Life countdown. When it hits zero the object is disabled.
		lifetime -= Time.deltaTime;
		if (lifetime < 0) {
			gameObject.SetActive (false);
			return;
		}

		// Update the object's positon. The two bearing vectors only contain x and z movement.
		pos += (bearing + missileBearing) * (speed * Time.deltaTime);

		// y movement is handled separately here for gravity and bouncing.
		vely += (gravity * Time.deltaTime);
		debrisPos.y += vely * Time.deltaTime;
		if (debrisPos.y < 0) {
			debrisPos.y = 0;
			vely *= -1;
		}

		// Move the whole object (only in x and z):
		transform.position = pos;
		// And move the debris chunk child (only y):
		debris.localPosition = debrisPos;
	}

	public void Activate(float x, float z, Vector3 _missileBearing) {
		// Called to switch on the object, move it to the spawn position, and store the
		// bearing vector of the missile that hit the enemy. Each debris chunk will move
		// a random amount in x and z but also move according to the missile bearing
		// which makes it look like the missile exploding added a force to the debris.
		pos.x = x;
		pos.z = z;
		debrisPos.y = 1;
		vely = Random.Range (0.5f, 1) * MAX_SPEED;
		transform.position = pos;
		lifetime = lifetimeReset;
		missileBearing = _missileBearing;
		gameObject.SetActive (true);
	}
}
