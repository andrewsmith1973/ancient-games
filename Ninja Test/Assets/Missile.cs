﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missile : MonoBehaviour {

	private const float SPEED = 5;
	public Vector3 pos;
	public Vector3 bearing;
	private Vector3 movement;
	public MissileCache.CachedMissile cachedMissile;
	private static Vector3 playerOffset;
	private Material startMaterial;
	public Material reboundMaterial;
	private Transform sphere;
	private MeshRenderer mr;
	public bool rebounded = false;

	// All Vector3 objects are pre-allocated so we don't need to allocate/deallocate memory
	// in play. Also reduces lookups as we don't need to pull the transform's position every
	// frame, only push it.

	void Start () {
	}

	public void Init() {
		sphere = transform.Find ("Cube");
		mr = sphere.GetComponent<MeshRenderer> ();
		startMaterial = mr.material;
	}

	public void Activate(float x, float z) {
		// Called after a pre-cached missile has been allocated. Set its position, its normalised
		// bearing to the player, reset material, and activate it.
		pos.x = x;
		pos.z = z;
		transform.position = pos;
		bearing.x = Player.pos.x - x;
		bearing.z = Player.pos.z - z;
		bearing.Normalize ();
		mr.material = startMaterial;
		rebounded = false;
		gameObject.SetActive (true);
		transform.LookAt (Player.pos);
	}
	
	void Update () {
		// Check if the missile is inside the player's shockwave. If it is, change the
		// bearing to the bearing set by the last swipe. Mark the missile as rebounded
		// so it can kill enemies. We only do this if the missile hasn't already rebounded.
		if (!rebounded) {
			playerOffset = Player.pos - pos;
			if (playerOffset.magnitude < Shockwave.size) {
				bearing = SwipeZone.newBearing;
				transform.forward = bearing;
				mr.material = reboundMaterial;
				rebounded = true;
				Audio.MissileDeflect ();
			}
		}

		// Move the missile position along the current bearing. Initially that will be
		// towards the player, but later may be redirected by the player swiping.
		float dist = SPEED * Time.deltaTime;
		movement = bearing * dist;
		pos += movement;

		// Check if the new position is outside the view frustrum. If it is we can free up
		// this missile to be re-used.
		if (Maths.IsPointOutsideViewFrustrum (pos)) {
			MissileCache.Release (cachedMissile);
			return;
		}

		// Update missile position
		transform.position = pos;
	}
}
