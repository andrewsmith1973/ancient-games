﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Points : MonoBehaviour {

	// Script for the points popup when an enemy dies. Moves the popup upwards until
	// life <= 0 and then disables it.

	private Vector3 pos;
	private const float RISE_SPEED = 1f;
	private const float LIFETIME = 2;
	private float life;

	void Start () {
	}
	
	void Update () {
		life -= Time.deltaTime;
		if (life <= 0) {
			gameObject.SetActive (false);
			return;
		}
		pos.y += RISE_SPEED * Time.deltaTime;
		transform.position = pos;
	}

	public void Activate(Vector3 _pos) {
		pos = _pos;
		pos.y = 1.5f;
		transform.position = pos;
		transform.LookAt (Camera.main.transform.position);
		life = LIFETIME;
		gameObject.SetActive (true);
	}
}
