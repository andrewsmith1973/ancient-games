Ninja Test for Ancient Games
by Andrew Smith (meejahor@gmail.com)

Made with Unity 2017.3.1f1
Designed for iOS devices in portrait orientation
(Won't work correctly in landscape orientation)

In the interests of creating a somewhat nice-looking and playable game within the time limit
I went with a basic sci-fi / Space Invaders style.

The main changes / additions in stage 3 of the process were:

1. Removed the swipe zone from the bottom of the screen and allowed the player to swipe
   anywhere on the screen.
2. Added a "shockwave" graphic when the player swipes so it makes sense to the player why
   the missiles are being deflected.
3. Simple special effects (debris and smoke trails)
4. Impact responses (the player gets nudged by missiles)
5. Sound effects

In the code for stages 1 and 2, I focused on showing that I can optimise and take the necessary
steps to eliminate garbage collection, etc. In stage 3 I focused on just "getting it done" so the
code isn't optimal, although I still kept GC to a minimum.

I didn't write any custom shaders as they weren't necessary. I do know how to write them. These two
videos from games I've worked on make extensive use of shaders:
https://www.youtube.com/watch?v=1RzrZFoa6N8
https://www.youtube.com/watch?v=gx3WJv0qMaQ
